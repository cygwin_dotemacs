;;nxml bit of elisp that blew up b/c of abbrev table issues??
  (add-hook 'nxml-mode-hook
       (lambda ()
         (define-key nxml-mode-map "r" 'newline-and-indent)
         (setq indent-tabs-mode nil)
         (setq local-abbrev-table nxml-mode-abbrev-table)
         (message "My nxml-mode customizations loaded")))

;;Trying to get .erb files to work with nXML and erb syntax highlighting via MMM
;; Found this mmm stuff here: http://www.ruby-forum.com/topic/61463
(require 'mmm-mode)
(require 'mmm-auto)
(setq mmm-global-mode 'maybe)
(setq mmm-submode-decoration-level 2)
(set-face-background 'mmm-output-submode-face  "DarkSlateBlue")
(set-face-background 'mmm-code-submode-face    "DarkSlateBlue")
(set-face-background 'mmm-comment-submode-face "DarkOliveGreen")
(mmm-add-classes
 '((erb-code
    :submode ruby-mode
    :match-face (("<%#" . mmm-comment-submode-face)
                 ("<%=" . mmm-output-submode-face)
                 ("<%"  . mmm-code-submode-face))
    :front "<%[#=]?"
    :back "%>"
    :insert ((?% erb-code       nil @ "<%"  @ " " _ " " @ "%>" @)
             (?# erb-comment    nil @ "<%#" @ " " _ " " @ "%>" @)
             (?= erb-expression nil @ "<%=" @ " " _ " " @ "%>" @))
    )))
(add-hook 'html-mode-hook
          (lambda ()
            (setq mmm-classes '(erb-code))
            (mmm-mode-on)))
(add-to-list 'auto-mode-alist '("\\.erb$" . html-mode))
(mmm-add-mode-ext-class 'html-mode "\\.php\\'" 'html-php)
(mmm-add-mode-ext-class 'html-mode "\\.js\\'" 'html-js)         
;;Nope, didn't work.  Found a slight variation on the above...         
                  

;;OK try another way to get mmm to grok rhtml.erb files...
;;Found here: http://www.naan.net/trac/wiki/emacs_rails
;;Must apply a patch for emacs 22 (see C:\cygwin\usr\share\emacs\site-lisp\mmm-mode-0.4.8\mmm-vars.el.patch)     
(require 'mmm-mode)
(require 'mmm-auto)
(setq mmm-global-mode 'maybe)
(setq mmm-submode-decoration-level 2)
(set-face-background 'mmm-output-submode-face  "LightBlue")
(set-face-background 'mmm-code-submode-face    "LightGray")
(set-face-background 'mmm-comment-submode-face "LightYellow")
(set-face-background 'mmm-special-submode-face "Yellow")

(mmm-add-classes
 '((erb-code
    :submode ruby-mode
    :match-face (("<%#" . mmm-comment-submode-face)
                 ("<%=" . mmm-output-submode-face)
                 ("<%"  . mmm-code-submode-face))
    :front "<%[#=]?" 
    :back "%>" 
    :insert ((?% erb-code       nil @ "<%"  @ " " _ " " @ "%>" @)
             (?# erb-comment    nil @ "<%#" @ " " _ " " @ "%>" @)
             (?= erb-expression nil @ "<%=" @ " " _ " " @ "%>" @))
    )))

(mmm-add-classes
 '((gettext
    :submode gettext-mode
    :front "_(['\"]"
    :face mmm-special-submode-face
    :back "[\"'])")))

(mmm-add-classes
 '((html-script
    :submode javascript-mode
    :front "<script>"
    :back "</script>")))

(add-to-list 'auto-mode-alist '("\\.rhtml$" . html-mode))


(add-hook 'html-mode-hook
	  (lambda ()
	    (setq mmm-classes '(erb-code html-js html-script gettext embedded-css))
	    (mmm-mode-on)))

(add-to-list 'mmm-mode-ext-classes-alist '(ruby-mode nil gettext))

(global-set-key [f8] 'mmm-parse-buffer)         
;;END MMM Test         