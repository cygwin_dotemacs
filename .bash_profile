##################################################
# .bash_profile file
##################################################
export HOME=/usr/will
unset MAILCHECK
shopt -s nocaseglob
export HISTCONTROL=ignoreboth

#Workaround for known cygwin/emacs 23 problem
export TZ=America/New_York
##################################################
# PATH
#=================================================

# Aliases
#=================================================
alias less='/bin/less -r'
alias ls='ls -sF --color=tty --show-control-chars'
alias la='ls -a'
alias ll='ls -l'
alias pu='pushd'
alias po='popd'
alias cdcyg='cd /cygdrive/c/cygwin-1.7/bin'
alias xterm='xterm -bg black -fg azure -font -*-fixed-medium-r-*-*-18-*-*-*-*-*-iso8859-* -geometry 70x24'

# Shorten commonly used rails commands
alias srv='script/server'
alias mig='rake db:migrate'
alias log='tail -f log/development.log'

# Ruby - have ruby use the Windows version
# OK, I'm going to abandon Windows Ruby in favor of cygwin Ruby
# alias ruby='/cygdrive/c/ruby/bin/ruby.exe'
# alias erb='/cygdrive/c/ruby/bin/erb.bat'
# alias gem='/cygdrive/c/ruby/bin/gem.bat'
# alias fxri='/cygdrive/c/ruby/bin/fxri.bat'
# alias gem_mirror='/cygdrive/c/ruby/bin/gem_mirror.bat'
# alias gem_server='/cygdrive/c/ruby/bin/gem_server.bat'
# alias gemhelp='/cygdrive/c/ruby/bin/gemhelp.bat'
# alias gemwhich='/cygdrive/c/ruby/bin/gemwhich.bat'
# alias irb='/cygdrive/c/ruby/bin/irb.bat'
# alias rake='/cygdrive/c/ruby/bin/rake.bat'
# alias ri='/cygdrive/c/ruby/bin/ri.bat'
# alias testrb='/cygdrive/c/bin/testrb.bat'
# alias update_rubygems='/cygdrive/c/ruby/bin/update_rubygems.bat'
# alias iconv='/cygdrivec/ruby/bin/iconv.exe'
# alias rubyw='/cygdrive/c/ruby/bin/rubyw.exe'

# set PATH so it includes user's private bin if it exists
if [[ -d ~/bin && ! $PATH =~ ~/bin ]] ; then
    export PATH=~/bin:"${PATH}"
fi


