(add-to-list 'load-path "/cygdrive/c/cygwin-1.7/usr/will")
(add-to-list 'load-path "/cygdrive/c/cygwin-1.7/usr/share/emacs/site-lisp")
(add-to-list 'load-path "~/.emacs.d") ;Don't know why I have the next conf dir as it doesn't seem necessary...
(add-to-list 'load-path "~/.emacs.d/conf")
(add-to-list 'load-path "/cygdrive/c/cygwin-1.7/usr/share/emacs/site-lisp/includes")

;;my elisp functions
(load "wl-elisp-funcs")

;I like the Consolas font!
;(set-frame-font "-microsoft-Consolas-normal-normal-normal-*-13-*-*-*-m-0-iso10646-1")
;this font is better
(set-frame-font "Bitstream Vera Sans Mono-10")

;Time settings
(setq display-time-day-and-date t)
(setq calendar-time-zone -360)
(setq calendar-standard-time-zone-name "EST")
(setq calendar-daylight-time-zone-name "EDT")
(display-time)

;; disable bell function
(setq ring-bell-function 'ignore)

;; arg >= 1 enable the menu bar.
(menu-bar-mode 0)

;Enable save history
(savehist-mode 1)

;;Disable the tool bar (with numeric ARG, display the tool bar if and only if ARG is positive)
(tool-bar-mode -1)

;; current buffer name in title bar
(setq frame-title-format "%b")

;;Disble scroll-bar-mode
(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))

;;Don't show the startup message
;(setq inhibit-startup-message t)

;; disable splash screen
(custom-set-variables
  ;; custom-set-variables was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.

;;; In case you have trouble byte-compiling color-theme files (uncomment these 2 variable customs)
;; '(max-specpdl-size 80000)
;; '(max-lisp-eval-depth 100000)
 '(autofit-frames-flag nil)
 '(dired-details-hidden-string "")
 '(color-theme-history-max-length 20)
 '(color-theme-history nil)
 '(display-time-24hr-format nil)
 '(ecb-cache-directory-contents nil)
 '(ecb-cache-directory-contents-not nil)
 '(ecb-layout-name "left13")
 '(ecb-options-version "2.32")
 '(ecb-primary-secondary-mouse-buttons (quote mouse-1--C-mouse-1))
 '(ecb-show-sources-in-directories-buffer (quote ("left7" "left13" "left14" "left15")))
 '(ecb-source-path (quote (("/usr/will/" "\$HOME") ("/usr/will/notes" "notes") ("/usr/will/myrailsapps/" "Current Rails Projects") ("/cygdrive/c/Projects" "Older Projects") ("/cygdrive/z/" "Z Drive"))))
 '(ecb-tip-of-the-day nil)
 '(inhibit-startup-screen t)
 '(org-agenda-files (quote ("~/notes/Personal/personal.org")))
 '(org-file-apps (quote ((auto-mode . emacs) (directory . "cygstart %s") ("\\.mm\\'" . default) ("\\.x?html?\\'" . default) ("\\.pdf\\'" . "cygstart %s") ("\\.doc\\'" . "cygstart %s") ("\\.xls\\'" . "cygstart %s") ("\\.ppt\\'" . "cygstart %s"))))
 '(ps-bottom-margin 25.51968503937008)
 '(ps-font-family (quote Times))
 '(ps-font-size (quote (12 . 12)))
 '(ps-header-font-size (quote (12 . 12)))
 '(ps-header-offset 18.346456692913385)
 '(ps-inter-column 26.69291338582677)
 '(ps-left-margin 12.69291338582677)
 '(ps-line-number-font-size 10)
 '(ps-print-header t)
 '(ps-print-only-one-header t)
 '(ps-right-margin 28.69291338582677)
 '(ps-spool-duplex nil)
 '(ps-top-margin 25.51968503937008)
 '(tramp-encoding-command-switch "-c")
 '(tramp-encoding-shell "/cygdrive/c/cygwin-1.7/bin/bash.exe"))

;(setq default-frame-alist
;      '((font . "Bitstream Vera Sans Mono-12")))

; Globals for frame-sizing functions
(defvar MY_INITIAL_WIDTH 92)
(defvar MY_INITIAL_HEIGHT 52)
(defvar MY_INITIAL_LEFT 4)
(defvar MY_INITIAL_TOP 30)

;;position emacs window in the left half of my screen
(setq initial-frame-alist
      '((user-position t) (left . 1288) (top . 27)
        (width . 80) (height . 50)
        ))

;;Clear the PID env var as emacs might inherit this if started from a bash shell
(setenv "PID" nil)

; Override nasty Xfce Home/will
(setq default-directory "~/" )

;;Set up printing using PDFCreator
(setenv "PRINTER" "PDFCreator")
(cond ((eq system-type 'cygwin)
   (setq ps-printer-name "PDFCreator")
   (setq ps-printer-name-option "-d")
   (setq ps-lpr-command "/bin/lpr")))

;;Something seems to have broken in the above (not working on other machine either)
;;Try something different--doesn't work, try something else...
;   (setenv "GS_LIB" "/cygdrive/c/Program Files/gs/gs8.64/lib;/cygdrive/c/Program Files/gs/gs8.64/fonts")
;   (setq ps-lpr-command "/cygdrive/c/Program Files/gs/gs8.64/bin/gswin32c.exe")
;   (setq ps-lpr-switches '("-q" "-dNOPAUSE" "-dBATCH" "-sDEVICE=mswinpr2"))
;   (setq ps-printer-name t)

;This works (prints with Courier, to change, see file:///C:/Program%20Files/gs/gs8.64/doc/Fonts.htm
;(setq ps-printer-name t)
;(setq ps-lpr-command "/cygdrive/c/Program Files/gs/gs8.64/bin/gswin32c.exe")
;(setq ps-lpr-switches '("-q" "-dNOPAUSE" "-dBATCH"
;                       "-sDEVICE=mswinpr2"
;                       "-sPAPERSIZE=a4"))

;;Popups (with reminders) currently not working
(defun djcb-popup (title msg &optional icon)
  "Show a popup if we're on X, or echo it otherwise; TITLE is the title
of the message, MSG is the context. Optionally, you can provide an ICON and
a sound to be played"

  (interactive)
  (when sound (shell-command
                (concat "mplayer -really-quiet " sound " 2> /dev/null")))
  (if (eq window-system 'x)
    (shell-command (concat "notify-send "

                     (if icon (concat "-i " icon) "")
                     " '" title "' '" msg "'"))
    ;; text only version

    (message (concat title ": " msg))))

;;Window-resizing Functions (note only work with a split window)
;;With split windows, I only want the second window as a reference so keep these resize functions handy.
(defun joc-enlarge-by-ten()
  "enlarges a window 10 lines"
  (interactive)
  (enlarge-window 10))

(defun joc-shrink-by-five()
  "shrinks a window 5 lines"
  (interactive)
  (shrink-window 5))

(defun uniquify-region ()
  "remove duplicate adjacent lines in the given region"
  (interactive)
  (shell-command-on-region (region-beginning) (region-end) "uniq" t)
)

;;Stevey's Effective Emacs keybindings
(global-set-key "\C-x\C-m" 'execute-extended-command)
(global-set-key "\C-c\C-m" 'execute-extended-command)
(global-set-key "\C-w" 'backward-kill-word)
(global-set-key "\C-x\C-k" 'kill-region)
(global-set-key "\C-c\C-k" 'kill-region)
(global-set-key "\C-xt" 'beginning-of-buffer)
(global-set-key "\C-xn" 'end-of-buffer)

;Need to make c-r a prefix key:
;(global-set-key (kbd "C-r f") 'rename-file-and-buffer)
;NOTE: Until I find myself using this a lot, don't bother keybinding


;;Aliases
(defalias 'bcf 'byte-compile-file)
(defalias 'ftf 'fit-frame)
(defalias 'isf 'insertfile)

;;Stevey's aliases
(defalias 'qrr 'query-replace-regexp)

;;Which file was I editing?
(define-key global-map "\C-c\C-of"
  '(lambda () (interactive nil) (message "%s" (buffer-file-name))))
;  '(lambda () (interactive nil) (funcall kill-new buffer-file-name replace yank-handler)))
;;My keybindings
(global-set-key [(f4)] 'ecb-activate)
(global-set-key [(shift f4)] 'ecb-hide-ecb-windows)
(global-set-key [(shift f3)] 'ecb-show-ecb-windows)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cg" 'goto-line)
; NOTE: C-\ was formerly bound to the toggle-input-method is an interactive compiled Lisp function in `mule-cmds.el'.
;;but since mule-cmds is for multi-lingual environments, I thought I'd use it for my replace-backslashes udf.
(global-set-key "\C-\\" 'replace-backslashes)

;;Load macros
(load-file "~/macros")

(setq-default ispell-program-name "aspell")

;;Useful package for viewing registers
(require 'list-register)
(global-set-key (kbd "C-x r v") 'list-register)

;color-theme package
(add-to-list 'load-path "~/.emacs.d/color-theme")
(require 'color-theme)
(eval-after-load "color-theme"
  '(progn
     (color-theme-initialize)
     ;(color-theme-hober) ; Not sure I like hober...use wombat instead defined in a fnc below.
     ))

;; Org mode settings
(require 'org-install)
(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))
(define-key global-map "\C-cl" 'org-store-link)
(define-key global-map "\C-ca" 'org-agenda)
(setq org-log-done t)

;;If I want project support in ido (like TextMate)
(defun ido-find-file-in-tag-files ()
  (interactive)
  (save-excursion
    (let ((enable-recursive-minibuffers t))
      (visit-tags-table-buffer))
    (ido-completing-read "Project file: "
                         (tags-table-files)
                         nil t)))

;http://www.emacswiki.org/emacs/ColorTheme
;Here are four alternative ways to cycle among color themes in a list. For illustration, they all use the same list.
(setq my-color-themes (list 'color-theme-billw 'color-theme-jsc-dark
                              'color-theme-sitaramv-solaris 'color-theme-resolve
                              'color-theme-classic 'color-theme-jonadabian-slate
                              'color-theme-kingsajz 'color-theme-shaman
                              'color-theme-subtle-blue 'color-theme-snowish
                              'color-theme-sitaramv-nt 'color-theme-wheat
                              'color-theme-railscasts 'color-theme-sunburst
                              'color-theme-colorful-obsolescence 'color-theme-twilight))

(setq theme-current my-color-themes)
(setq color-theme-is-global nil) ; Initialization

(defun my-theme-set-default () ; Set the first row
  (interactive)
  (setq theme-current my-color-themes)
  (funcall (car theme-current)))

(defun my-describe-theme () ; Show the current theme
  (interactive)
  (message "%s" (car theme-current)))

   ; Set the next theme (fixed by Chris Webber - tanks)
(defun my-theme-cycle ()
  (interactive)
  (setq theme-current (cdr theme-current))
  (if (null theme-current)
      (setq theme-current my-color-themes))
      (funcall (car theme-current))
      (message "%S" (car theme-current)))

(my-theme-set-default)
(global-set-key [f12] 'my-theme-cycle)

(defun color-theme-undo ()
  (interactive)
  (color-theme-reset-faces)
  (color-theme-snapshot))

;; backup current color theme
(fset 'color-theme-snapshot (color-theme-make-snapshot))
;;change color theme
;;M-x color-theme-undo

;Found this color theme
(require 'color-theme-ahei)

;;tramp support
(require 'tramp)
(setq tramp-default-method "sshx")
;;(setq tramp-debug-buffer t)
;;(setq tramp-chunksize 500)
;;(setq shell-prompt-pattern "^H")

;dired-details+ is all you need to get dired-details.el loaded, plus it gives you some extras
(require 'dired-details+)

;fit-frame.el Resize a frame.  In particular, fit a frame to its buffers.
(require 'fit-frame)
(add-hook 'after-make-frame-functions 'fit-frame)

;dired-lis  Lets you isearch in dired-mode using a similar process that ido uses
(require 'dired-lis)

;;Resize frames automatically with autofit-frame.el
(require 'autofit-frame)
(add-hook 'after-make-frame-functions 'fit-frame)


;; For Common Lisp stuff
(require 'cl)

;;use mmm mode
(require 'mmm-mode)
(require 'mmm-auto)
(setq mmm-global-mode 'maybe)
(setq mmm-submode-decoration-level 2)
(set-face-background 'mmm-output-submode-face  "LightBlue")
(set-face-background 'mmm-code-submode-face    "LightGray")
(set-face-background 'mmm-comment-submode-face "LightYellow")
(set-face-background 'mmm-special-submode-face "Yellow")

(mmm-add-classes
 '((erb-code
    :submode ruby-mode
    :match-face (("<%#" . mmm-comment-submode-face)
                 ("<%=" . mmm-output-submode-face)
                 ("<%"  . mmm-code-submode-face))
    :front "<%[#=]?"
    :back "%>"
    :insert ((?% erb-code       nil @ "<%"  @ " " _ " " @ "%>" @)
             (?# erb-comment    nil @ "<%#" @ " " _ " " @ "%>" @)
             (?= erb-expression nil @ "<%=" @ " " _ " " @ "%>" @))
    )))

(mmm-add-classes
 '((gettext
    :submode gettext-mode
    :front "_(['\"]"
    :face mmm-special-submode-face
    :back "[\"'])")))

(mmm-add-classes
 '((html-script
    :submode javascript-mode
    :front "<script>"
    :back "</script>")))

(add-to-list 'auto-mode-alist '("\\.rhtml$" . html-mode))
;;WSL--I added the following line as an attempt to include those Rails 2.1 rhtml.erb files...
(add-to-list 'auto-mode-alist '("\\.erb$" . html-mode))

; Major mode for editing YAML files
(require 'yaml-mode)
(add-to-list 'auto-mode-alist '("\\.yml$" . yaml-mode))

(add-hook 'html-mode-hook
          (lambda ()
            (setq mmm-classes '(erb-code html-js html-script gettext embedded-css))
            (mmm-mode-on)))

(add-to-list 'mmm-mode-ext-classes-alist '(ruby-mode nil gettext))

(global-set-key [f8] 'mmm-parse-buffer)
;;END MMM Test

;;use ido (interactive do)
(require 'ido)
(ido-mode t)

;;load line-num.el
(require 'line-num "line-num.el")

;; Load cedet NOTE: Why do I need this?  Maybe just a require
;;(load-file "/cygdrive/c/cywin-1.7/usr/share/emacs/site-lisp/cedet-1.0pre4/common/cedet.el")
(require 'cedet)

(define-key-after (lookup-key global-map [menu-bar tools])
      [speedbar] '("Speedbar" . speedbar-frame-mode) [calendar])

;;turn font-lock on automatically
(if (fboundp 'global-font-lock-mode)
    (global-font-lock-mode 1)        ; GNU Emacs
  (setq font-lock-auto-fontify t))   ; XEmacs

;; Enabling various SEMANTIC minor modes.  See semantic/INSTALL for more ideas.
;; Select one of the following:

;; * This enables the database and idle reparse engines
;;(semantic-load-enable-minimum-features)

;;Bind Refresh to F5 key
(global-set-key [f5]
'(lambda () "Refresh the buffer from the disk (prompt of modified)."
(interactive)
(revert-buffer t (not (buffer-modified-p)) t)))

;; * This enables some tools useful for coding, such as summary mode
;;   imenu support, and the semantic navigator
;;(semantic-load-enable-code-helpers)

;; * This enables even more coding tools such as the nascent intellisense mode
;;   decoration mode, and stickyfunc mode (plus regular code helpers)
(semantic-load-enable-gaudy-code-helpers)

;; * This turns on which-func support (Plus all other code helpers)
;; (semantic-load-enable-excessive-code-helpers)

;; This turns on modes that aid in grammar writing and semantic tool
;; development.  It does not enable any other features such as code
;; helpers above.
;; (semantic-load-enable-semantic-debugging-helpers)

;;Load ECB
;;If you want to load the ECB first after starting it by ecb-activate (faster loading)
(require 'ecb)

;;This loads all available autoloads of ECB, e.g. ecb-activate, ecb-minor-mode, ecb-byte-compile and ecb-show-help.
(require 'ecb-autoloads)

;;Added automatically after activating ECB the 1st time.



(custom-set-faces
  ;; custom-set-faces was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 )

;; keep searching throughout the file
(require 'find-recursive)

;;; ri-documentation
(add-to-list 'load-path "/cygdrive/c/cywin-1.7/usr/share/emacs/site-lisp/ri")
(setq ri-ruby-script "/cygdrive/c/cywin-1.7/usr/share/emacs/site-lisp/ri/ri-emacs.rb")
(autoload 'ri "/cygdrive/c/cywin-1.7/usr/share/emacs/site-lisp/ri/ri-ruby.el" nil t)
;;Bind the ri command to a key
;;Method/class completion is also available
(add-hook 'ruby-mode-hook (lambda ()
    (local-set-key "\C-cd" 'ri)
    (local-set-key "\C-cf" 'ri-ruby-complete-symbol)
    (local-set-key "\C-ca" 'ri-ruby-show-args)
))

;;use the ruby-mode for ruby (*.rb) files
(add-to-list 'load-path "/cygdrive/c/cywin-1.7/usr/share/emacs/site-lisp/ruby-mode")
(autoload 'ruby-mode "ruby-mode" "Ruby editing mode." t)
(add-to-list 'auto-mode-alist '("\\.rb$" . ruby-mode))
(add-to-list 'interpreter-mode-alist '("ruby" . ruby-mode))
(autoload 'run-ruby "inf-ruby"
    "Run an inferior Ruby process")
(autoload 'inf-ruby-keys "inf-ruby"
    "Set local key defs for inf-ruby in ruby-mode")
(add-hook 'ruby-mode-hook
    '(lambda ()
        (inf-ruby-keys)))

;;use electric mode for ruby
(require 'ruby-electric)
(add-hook 'ruby-mode-hook (lambda () (ruby-electric-mode t)))

;;FlymakeRuby
;(require 'flymake)
;; Change the default colors :)
;(set-face-background 'flymake-errline "red4")
;(set-face-background 'flymake-warnline "dark slate blue")

;NOTE: This includes the flymake-create-temp-in-system-tempdir fnc which
; keeps temp files in the system temp dir instead of the same directory as the file.
; but until that becomes a problem, I'm going to use the flymake for ruby configuration included in
; C:\cygwin-1.7\usr\share\emacs\site-lisp\rails\rails-ruby.el
;(require 'flymake-ruby)
;(add-hook 'ruby-mode-hook 'flymake-ruby-load)

;;emacs-rails
(setq load-path (cons "/cygdrive/c/cywin-1.7/usr/share/emacs/site-lisp/rails" load-path))
(require 'snippet)
(require 'rails)

;;use the 4gl-mode for Informix .4gl files
(autoload '4gl-mode "4gl-mode" "Informix 4gl editing mode." t)
(add-to-list 'auto-mode-alist '("\.4gl$" . 4gl-mode))
(add-to-list 'interpreter-mode-alist '("4gl" . 4gl-mode))

;;; Set up PSGML
;; Add PSGML to load-path so Emacs can find it.
;; Note the forward slashes in the path... this is platform-independent so I
;; would suggest using them over back slashes. If you use back slashes, they
;; MUST BE doubled, as Emacs treats backslash as an escape character.
(setq load-path (append (list nil "/cygdrive/c/cywin-1.7/usr/share/emacs/site-lisp/psgml/") load-path))

;; Use PSGML for sgml and xml major modes.  No, use Jim Clark's nXML major mode
;;(autoload 'sgml-mode "psgml" "Major mode to edit SGML files." t)
;;(autoload 'xml-mode "psgml" "Major mode to edit XML files." t)

;;nXML Mode by Jim Clark
;;For documentation of this mode, see http://www.nmt.edu/tcc/help/pubs/nxml/
;; Add the nxml files to emacs's search
;; path for loading:
;;--
(setq load-path
        (append load-path
                '("~/.emacs.d/plugins/nxml-mode-20041004/")))
;;--
;; Make sure nxml-mode can autoload
;;--
;;File rng-auto.el will not be loaded until needed (for example by opening some .xml file)
(autoload 'nxml-mode "rng-auto.el" nil t)


;;--
;; Load nxml-mode for files ending in .xml, .xsl, .rng, .xhtml
;;--
  (setq auto-mode-alist
                (cons '("\\.\\(xml\\|xsl\\|rng\\|xhtml\\|xsd\\)\\'" . nxml-mode)
              auto-mode-alist))


;; Use cperl-mode instead of the default perl-mode
;;brevity is the soul of wit
(defalias 'perl-mode 'cperl-mode)

;;Customize the tabs for CPerl
(add-hook 'cperl-mode-hook 'n-cperl-mode-hook t)
(defun n-cperl-mode-hook ()
  (setq cperl-indent-level 4)
  (setq cperl-continued-statement-offset 0)
  (setq cperl-extra-newline-before-brace t)
  (set-face-background 'cperl-array-face "wheat")
  (set-face-background 'cperl-hash-face "wheat")
  )
;;Customizing CPerl mode
;;Disable underscore spacebar feature
(setq cperl-invalid-face nil)
;;Expand keywords like foreach, while, etc...
(setq cperl-electric-keywords t)
;;electic parenthesis
(setq cperl-electric-parens t)
;;automatically add newlines before and after braces, colons, semicolons
(setq cperl-auto-newline nil)

;; cmd-mode is automatically activated when you open a file whose
;; extention is "cmd" or "bat".
(autoload 'cmd-mode "cmd-mode" "CMD mode." t)
(setq auto-mode-alist (append '(("\\.\\(cmd\\|bat\\)$" . cmd-mode))
                                  auto-mode-alist))

;;Per Steve Yegge, he always, always, always runs a shell in emacs--until I need to, I'm turning this off...
;;(shell)

;;Yegge's JavaScript mode
(autoload 'js2-mode "js2" nil t)
(add-to-list 'auto-mode-alist '("\\.js$" . js2-mode))
(setq js2-basic-offset 2)
(setq js2-use-font-lock-faces t)

;;More Yegge Stuff (fonts)  currently not working (parchment-screen nope...linux-font...nope)
(global-font-lock-mode t)
; (parchment-screen)
; (set-default-font linux-font)
; (set-mouse-color "black")

;;Highlights erroneous lines and displays associated error messages.
;;set colors in PSGML mode:
(setq screenshots nil)   ; t for screenshot color settings, else nil

(make-face 'sgml-comment-face)
(make-face 'sgml-start-tag-face)
(make-face 'sgml-end-tag-face)
(make-face 'sgml-entity-face)
(make-face 'sgml-doctype-face)

(cond ((equal screenshots nil)
   (set-face-foreground 'sgml-comment-face "FireBrick")
   (set-face-foreground 'sgml-start-tag-face "SlateBlue")
   (set-face-foreground 'sgml-end-tag-face "SlateBlue")
   (set-face-background 'sgml-entity-face "SlateBlue")
   (set-face-foreground 'sgml-entity-face "Red")
   (set-face-foreground 'sgml-doctype-face "FireBrick")
  )
  ((equal screenshots t)
   ; This set for screen shots
   (set-background-color "White")
   (set-face-foreground 'sgml-comment-face "White")  ; Comments: white on
   (set-face-background 'sgml-comment-face "Gray")   ; gray.
   (set-face-background 'sgml-start-tag-face "Gray") ; Tags: black (default)
   (set-face-background 'sgml-end-tag-face "Gray")   ; on gray.
   (set-face-foreground 'sgml-entity-face "White")   ; Entity references:
   (set-face-background 'sgml-entity-face "Black")   ; white on black.
  )
(t nil))

(setq sgml-set-face t)  ; without this, all SGML text is in same color
(setq sgml-markup-faces
   '((comment   . sgml-comment-face)
     (start-tag . sgml-start-tag-face)
     (end-tag   . sgml-end-tag-face)
     (doctype   . sgml-doctype-face)
     (entity    . sgml-entity-face)))


(defvar sgml-font-lock-keywords
  '(; Highlight the text between these tags in SGML mode.
      ("<indexterm[^>]*>" . font-lock-comment-face)
      ("</indexterm>" . font-lock-comment-face)
      ("<primary[^<]+</primary>" . font-lock-comment-face)
      ("<secondary[^<]+</secondary>" . font-lock-comment-face)
      ("<see[^<]+</see>" . font-lock-comment-face)
      ("<seealso[^<]+</seealso>" . font-lock-comment-face)
   )
  "Additional expressions to highlight in SGML mode.")

  (setq font-lock-defaults '(sgml-font-lock-keywords t))

;;Emulate the clear command from bash to clear the entire shell buffer
(add-hook 'shell-mode-hook 'n-shell-mode-hook)
(defun n-shell-mode-hook ()
  "12Jan2002 - sailor, shell mode customizations."
  (local-set-key '[up] 'comint-previous-input)
  (local-set-key '[down] 'comint-next-input)
  (local-set-key '[(shift tab)] 'comint-next-matching-input-from-input)
  (setq comint-input-sender 'n-shell-simple-send)
  )

(defun n-shell-simple-send (proc command)
  "17Jan02 - sailor. Various commands pre-processing before sending to shell."
  (cond
   ;; Checking for clear command and execute it.
   ((string-match "^[ \t]*clear[ \t]*$" command)
    (comint-send-string proc "\n")
    (erase-buffer)
    )
;; Checking for man command and execute it.
   ((string-match "^[ \t]*man[ \t]*" command)
    (comint-send-string proc "\n")
    (setq command (replace-regexp-in-string "^[ \t]*man[ \t]*" "" command))
    (setq command (replace-regexp-in-string "[ \t]+$" "" command))
    ;;(message (format "command %s command" command))
    (funcall 'man command)
    )
   ;; Send other commands to the default handler.
   (t (comint-simple-send proc command))
   )
  )

(add-to-list 'load-path "/cygdrive/c/cywin-1.7/usr/share/emacs/site-lisp/rhtml/")
    (require 'rhtml-mode)

; Install mode-compile to give friendlier compiling support!
(autoload 'mode-compile "mode-compile"
"Command to compile current buffer file based on the major mode" t)
(global-set-key "\C-cc" 'mode-compile)
(autoload 'mode-compile-kill "mode-compile"
 "Command to kill a compilation launched by `mode-compile'" t)
(global-set-key "\C-ck" 'mode-compile-kill)

;;more from Yegge's "My .emacs file" blog entry
(defun swap-windows ()
 "If you have 2 windows, it swaps them." (interactive) (cond ((not (= (count-windows) 2)) (message "You need exactly 2 windows to do this."))
 (t
 (let* ((w1 (first (window-list)))
         (w2 (second (window-list)))
         (b1 (window-buffer w1))
         (b2 (window-buffer w2))
         (s1 (window-start w1))
         (s2 (window-start w2)))
 (set-window-buffer w1 b2)
 (set-window-buffer w2 b1)
 (set-window-start w1 s2)
 (set-window-start w2 s1)))))

;; Never understood why Emacs doesn't have this function.
(defun rename-file-and-buffer (new-name)
 "Renames both current buffer and file it's visiting to NEW-NAME." (interactive "sNew name: ")
 (let ((name (buffer-name))
        (filename (buffer-file-name)))
 (if (not filename)
        (message "Buffer '%s' is not visiting a file!" name)
 (if (get-buffer new-name)
         (message "A buffer named '%s' already exists!" new-name)
        (progn   (rename-file name new-name 1)   (rename-buffer new-name)        (set-visited-file-name new-name)        (set-buffer-modified-p nil)))))) ;;

;; Never understood why Emacs doesn't have this function, either.
(defun move-buffer-file (dir)
 "Moves both current buffer and file it's visiting to DIR." (interactive "DNew directory: ")
 (let* ((name (buffer-name))
         (filename (buffer-file-name))
         (dir
         (if (string-match dir "\\(?:/\\|\\\\)$")
         (substring dir 0 -1) dir))
         (newname (concat dir "/" name)))

 (if (not filename)
        (message "Buffer '%s' is not visiting a file!" name)
 (progn  (copy-file filename newname 1)
         (delete-file filename)
         (set-visited-file-name newname)
         (set-buffer-modified-p nil)     t))))
