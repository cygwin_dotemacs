;wl_elisp.el
(defun new-count-words-buffer ()
  "Count the number of words in the current buffer;
print a message in the minibuffer with the result."
  (interactive)
  (save-excursion
    (let ((count 0))
      (goto-char (point-min))
      (while (< (point) (point-max))
        (forward-word 1)
        (setq count (1+ count)))
      (message "buffer contains %d words." count))))

(defun wombat () "Load the wombat color theme."
        (interactive)
        (if (file-exists-p "~/.emacs.d/color-theme/themes/color-theme-wombat.el")
        (load-file "~/.emacs.d/color-theme/themes/color-theme-wombat.el")))

(defun reload () "Reloads .emacs interactively"
        (interactive)
    (if (file-exists-p "~/.emacs")
    (load-file "~/.emacs")))

(defun count-words-buffer ()
  "Count the number of words in the current buffer;
print a message in the minibuffer with the result."
  (interactive)
  (save-excursion
    (let ((count 0))
      (goto-char (point-min))
      (while (< (point) (point-max))
        (forward-word 1)
        (setq count (1+ count)))
      (message "buffer contains %d words." count))))

(defun replace-backslashes () "Replace the backslashes with slashes."
        (interactive)
        (save-excursion)
        (replace-regexp "\\\\" "/" nil
        (if (and transient-mark-mode mark-active) (region-beginning))
                (if (and transient-mark-mode mark-active) (region-end))))

                
;;;;;;;;;;;;;;;;;;;;;;;;;;;;	EXPLORATORY STUFF (MIGHT NOT WORK)   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;                
;;;======================================================================
;;; frame sizing functions http://www.emacswiki.org/emacs/McMahanEmacsMacros
;;;======================================================================
(	defun frame-fix ()
  "Restore the frame back to the initial height, width and position as
set up in the .emacs
uses the global variables:
   `MY_INITIAL_WIDTH'
   `MY_INITIAL_HEIGHT'
   `MY_INITIAL_LEFT'
   `MY_INITIAL_TOP'
defined in the .emacs file"
  (interactive)
  (set-frame-size (selected-frame) MY_INITIAL_WIDTH MY_INITIAL_HEIGHT)
  (set-frame-position (selected-frame) MY_INITIAL_LEFT MY_INITIAL_TOP)
  )

(defun f-adjust (&optional frame-width &optional left-offset &optional screen-width &optional screen-height)
  "Enlarge the selected frame to fill a sizeable portion of the screen, based on the current screen resolution.
- frame-width is the number of columns in the desired frame
- screen-width is the number of pixels in the screen. Defaults to the primary screen resolution
- left-offset is the offset in pixels to place the
  screen from the left. Good for moving a frame to the
  secondary screen to the right of the primary screen by just
  using the width of the primary screen in this field."

  (if (null screen-width)
      (setq screen-width (display-pixel-width)))

  (if (null screen-height)
      (setq screen-height (display-pixel-height)))

  (if (null frame-width)
      (setq frame-width (- (/ screen-width (frame-char-width)) 10)))

  (if (null left-offset)
      (setq left-offset 0))

  (setq frame-height (/ (- screen-height MY_HEIGHT_ADJUST) (frame-char-height)))

  ; set the new frame size to get an accurate frame-pixel-width
  (set-frame-size (selected-frame) frame-width frame-height)

  ; fix the right side of the emacs frame 25 pixels from the edge plus any left-offset value
  (setq frame-left (+ (- (- screen-width (frame-pixel-width)) 25) left-offset))

  ; set the frame position with the yoffset calculated from the screen height
  (set-frame-position (selected-frame)
    frame-left (/ (- screen-height (+ (frame-pixel-height) MY_WINDOW_MGR_ADJUST)) 2))
  )

(defvar frame-enlarged 'nil 
  "t if the current frame is enlarged (frame-enlarge has been called), nil otherwise")

(defun frame-enlarge ()
  "Enlarge the frame to fill a sizable portion of the current screen"
  (interactive)
  (setq frame-enlarged t)
  (if (> (frame-parameter nil 'left) (display-pixel-width))
      (f-adjust nil (display-pixel-width) 1440 900)
  (f-adjust)))

(defun frame-adjust ()
  "Adjust the frame size for the current frame to the appropriate height and default MY_INITIAL_WIDTH value"
  (interactive)
  (setq frame-enlarged nil)
  (if (> (frame-parameter nil 'left) (display-pixel-width))
      (f-adjust MY_INITIAL_WIDTH (display-pixel-width) 1440 900)
    (f-adjust MY_INITIAL_WIDTH nil nil nil)))

(defun frame-left ()
  "Move the current frame to the left (primary) monitor"
  (interactive)
  (if frame-enlarged
      (f-adjust)
    (f-adjust MY_INITIAL_WIDTH nil nil nil)))

(defun frame-right ()
  "Move the current frame to the right (secondary) monitor with a 1280 x 960 resolution"
  (interactive)
  (if frame-enlarged
      (f-adjust nil (display-pixel-width) 1280 960)
    (f-adjust MY_INITIAL_WIDTH (display-pixel-width) 1280 960)))

(global-set-key "\C-cfa" 'frame-adjust)
(global-set-key "\C-cfe" 'frame-enlarge)
(global-set-key "\C-cfl" 'frame-left)
(global-set-key "\C-cfr" 'frame-right)


;;;======================================================================
;;; From: lawrence mitchell <wence@gmx.li>
;;; Find the function under the point in the elisp manual
;;;
;;; C-h TAB runs the command info-lookup-symbol
;;;    which is an interactive autoloaded Lisp function in `info-look'.
;;; [Arg list not available until function definition is loaded.]
;;;
;;; Display the definition of SYMBOL, as found in the relevant manual.
;;; When this command is called interactively, it reads SYMBOL from the minibuffer.
;;; In the minibuffer, use M-n to yank the default argument value
;;; into the minibuffer so you can edit it.
;;; The default symbol is the one found at point.
;;;
;;; With prefix arg a query for the symbol help mode is offered.
;;;======================================================================
(defun find-function-in-elisp-manual (function)
  (interactive
   (let ((fn (function-called-at-point))
	 (enable-recursive-minibuffers t)
	 val)
     (setq val
	   (completing-read
	    (if fn
		(format "Find function (default %s): " fn)
	      "Find function: ")
	    obarray 'fboundp t nil nil (symbol-name fn)))
     (list (if (equal val "")
	       fn
	     val))))
  (Info-goto-node "(elisp)Index")
  (condition-case err
      (progn
	(search-forward (concat "* "function":"))
	(Info-follow-nearest-node))
    (error (message "`%s' not found" function))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;	OLDER EXPLORATORY STUFF		;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; NOTE:  I attempted to emulate a solution I found here:
;http://stackoverflow.com/questions/1037545/how-to-replace-forwardslashes-with-backslashes-in-a-string-in-lisp-for-emacs
; but failed.  I ran replace-regexp on a region and hit C-x ESC ESC and pasted
; mini-buffer into the above func:
;       (replace-regexp "\\\\" "/" nil
;       (if (and transient-mark-mode mark-active) (region-beginning))
;                   (if (and transient-mark-mode mark-active) (region-end)))
; Currently examining this tutorial so I can figure out why the stackoverflow solution failed.
;http://xahlee.org/emacs/lisp_regex_replace_func.html
(defmacro defreplacer (name description search-for replace-with chord)
  `(progn
     (defun ,name (n)
       ,description
       (interactive "p")
       (query-replace-regexp ,search-for
                             ,replace-with
                             nil
                             (if (and transient-mark-mode mark-active)
                                 (region-beginning))
                             (if (and transient-mark-mode mark-active)
                                 (region-end))))
     (global-set-key (kbd ,chord) ',name)))

(defreplacer pull-text-from-semi-colons
 "Remove text from between two semi-colon signs."
  "[ ]*;\\([a-z]*\\);[ ]*" ; use double back slash to 'escape' in quotes
  "\\1"
  "C-c ;")
;The above came from here:
;http://lispy.wordpress.com/category/regular-expressions/
;It's a good example of how you can set up text processing routines
;Using defmacro instead of just a defun lets you defines the key binding at the same time that the replacement function is defined.  This also has it's drawbacks, lispy gives this as his rationale:
;I went with a macro because once I get my regular expression from re-builder, I wanted to be able to write the code for everything as quickly as possible. With “defreplacer”, all I needed was four arguments and I was good to go.

(defun remove-blank-lines (n)
"Removes blank lines from a buffer"
(interactive "p") ; n = value passed by the C-u “universal argument”
(query-replace-regexp "

+" "
" nil
(if (and transient-mark-mode mark-active) (region-beginning))
(if (and transient-mark-mode mark-active) (region-end))))


(defun kill-new (string &optional replace yank-handler)
  "Make STRING the latest kill in the kill ring.
     Set `kill-ring-yank-pointer' to point to it.

     If `interprogram-cut-function' is non-nil, apply it to STRING.
     Optional second argument REPLACE non-nil means that STRING will replace
     the front of the kill ring, rather than being added to the list.
     ..."

  (if (> (length string) 0)
      (if yank-handler
          (put-text-property 0 (length string)
                             'yank-handler yank-handler string))
    (if yank-handler
        (signal 'args-out-of-range
                (list string "yank-handler specified for empty string"))))
  (if (fboundp 'menu-bar-update-yank-menu)
      (menu-bar-update-yank-menu string (and replace (car kill-ring))))
  (if (and replace kill-ring)
      (setcar kill-ring string)
    (push string kill-ring)
    (if (> (length kill-ring) kill-ring-max)
        (setcdr (nthcdr (1- kill-ring-max) kill-ring) nil)))
  (setq kill-ring-yank-pointer kill-ring)
  (if interprogram-cut-function
      (funcall interprogram-cut-function string (not replace))))
;http://www.gnu.org/software/emacs/emacs-lisp-intro/html_node/
; specifically
;http://www.gnu.org/software/emacs/emacs-lisp-intro/html_node/kill_002dnew-function.html

;NOTE:  I tried to embed the above fnc into the C-c C-o f bind which puts a buffer's file
;and location in the mini-buffer.  My logic was that it would put the buffer-file-name in
;the kill ring instead.  IT didn't work.


;;Try using this to solve the above:
;;;======================================================================
;;; replaces the path to windows format and puts it into the clipboard
;;;======================================================================
(define-key minibuffer-local-completion-map
	    "\M-\C-w" 'find-file-copy-path-win)

(defun find-file-copy-path-win ()
 "While at the find-file prompt, replaces the file path with the
corresponding Windows backslash style AND copies the replacement to the
clipboard.
"
 (interactive) ; required b/c it's a keypress
 ;; FIX: Would like to validate the input here, but (minibuffer-complete)
 ;; <f> is too verbose and too noisy. I will just trust that the user
 ;; wanted to copy exactly what is shown in the minibuffer.
 ;;(if (not (eq last-command 'minibuffer-complete)) ;;
 ;;       (minibuffer-complete))                    ;;
 (save-excursion
  (let ((mini-buf (current-buffer))
	(string (buffer-string))
	(temp-buf (get-buffer-create " find-file-copy-pathclip-buf")))
   (set-buffer temp-buf)
   (insert-string string)
   ;; If prefixed with a tilde, replace it with a full path to HOME
   (goto-char (point-min))
   (while (search-forward-regexp "^~" nil t)
    (replace-match (getenv "HOME") nil t))
   ;; Do unix->dos filename separator conversion.
   (goto-char (point-min))
   (while (search-forward "/" nil t)
    (replace-match "\\" nil t))
   ;; Copy to the windows clipboard.
   (clipboard-kill-ring-save (point-min) (point-max))
   (kill-buffer temp-buf)
   ;; Show the user that you made the change
   (set-buffer mini-buf)
   (delete-region (point-min) (point-max))
   (clipboard-yank)
   )))